package com.example.order.service;


import lombok.extern.java.Log;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;

import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.config.EnableStateMachineFactory;
import org.springframework.statemachine.config.StateMachineBuilder;
import org.springframework.statemachine.config.StateMachineConfigurerAdapter;

import org.springframework.statemachine.config.StateMachineFactory;
import org.springframework.statemachine.config.builders.StateMachineConfigurationConfigurer;
import org.springframework.statemachine.config.builders.StateMachineStateConfigurer;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;
import org.springframework.statemachine.listener.StateMachineListenerAdapter;

import org.springframework.statemachine.state.State;
import org.springframework.stereotype.Component;
import org.springframework.context.annotation.Bean;
import org.springframework.statemachine.guard.Guard;
/**
 *
@Autor Ruben Barrera
dependencias:
lombok:
     para el @Log
    log.info
State Machine
 */

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}

/**
 * ESTADO 1
 */
    enum OrderEvents {

        INICIA_PROCESO,
        ELEVAR,
        REVISAR,
        CERRRAR,
        TERMINAR,

    }

    enum OrderStates{

    CREADA,
        EN_PROCESO,
        ELEVADA,
        TERMINADA,
        NUEVAPROVI,
        MEDIA,
        FINAPROVI,
        NºREFERENCIA,
        VALIDACION
//        E1,
//        E2,

    }

/**
 *  ESTADO 2
 */
enum OrderStates2{
    inicio,
    proceso,
    elevado,
    terminado,
    E1,
    E2,
}




    /**
     *  todo lo que pongas en el método run se ejecutará al inicializar el contexto de spring
    * */


@Log
@Component


 class Runner implements ApplicationRunner{



    private final StateMachineFactory <OrderStates, OrderEvents> factory;

        Runner(StateMachineFactory<OrderStates, OrderEvents> factory) {
            this.factory = factory;
        }
//        private final StateMachineFactory <OrderStates2, OrderEvents> factory2;




        @Override
    public void run(ApplicationArguments args) throws Exception {
        StateMachine<OrderStates,OrderEvents> machine = this.factory.getStateMachine("1");
        machine.start();
       log.info ("Estado actual:" + machine.getState().getId().name());
            machine.sendEvent(OrderEvents.TERMINAR);
            log.info("Estado actual: "+machine.getState().getId().name());
            machine.sendEvent(OrderEvents.INICIA_PROCESO);
            log.info("Estado actual: "+machine.getState().getId().name());


//            machine.sendEvent(OrderEvents.ELEVAR);
//            log.info("Estado actual: "+machine.getState().getId().name());
//
//            machine.sendEvent(OrderEvents.TERMINAR);
//            log.info("Estado actual: "+machine.getState().getId().name());




//            StateMachine<OrderStates2, OrderEvents> machine2 = this.factory2.getStateMachine("1");
//
//            log.info ("Estado actual:" + machine.getState().getId().name());
//            machine2.sendEvent(OrderEvents.INICIA_PROCESO);
//            log.info("Estado actual: "+machine2.getState().getId().name());
//
//            machine2.sendEvent(OrderEvents.ELEVAR);
//            log.info("Estado actual: "+machine2.getState().getId().name());
//




        }


// investigar el tema del guard para las restricciones
}


    @Log
    @Configuration
    @EnableStateMachineFactory
            /**
             *  StateMachineConfigureAdapter  es una clase de la dependencia stateMachine
             */
    class SimpleEnumStatemachineConfiguration extends StateMachineConfigurerAdapter<OrderStates, OrderEvents> {
         boolean test = false;
         String nombre= "test";
         String apellido= "test";
    /**
     *Configuracion de las Transiciones
     * Condicionar por estados

     */

      @Override
        public void configure(StateMachineTransitionConfigurer< OrderStates, OrderEvents> transitions) throws Exception {
          /** Se deben poner todos los caminos posibles o el estado quedaria estancado
           *            */

          transitions
                  .withExternal()
//                  .source(OrderStates.CREADA).target(OrderStates.EN_PROCESO).event(OrderEvents.INICIA_PROCESO)
//                  .guard(Nguard(true)) // si el contexto no es verdadero no pasa a enproceso
//                  .and()
//                  .withExternal().source(OrderStates.EN_PROCESO).target(OrderStates.ELEVADA).event(OrderEvents.ELEVAR)
//                  .and()
//                  .withExternal().source(OrderStates.ELEVADA).target(OrderStates.TERMINADA).event(OrderEvents.TERMINAR)
//                  .and()
//                  .withExternal().source(OrderStates.ELEVADA).target(OrderStates.EN_PROCESO).event(OrderEvents.REVISAR)
//                  .and()
//                  .withExternal().source(OrderStates.EN_PROCESO).target(OrderStates.TERMINADA).event(OrderEvents.CERRRAR);

                  .source(OrderStates.CREADA).target(OrderStates.TERMINADA).event(OrderEvents.TERMINAR)
                  .guard(Nguard(true)) // si el contexto no es verdadero no pasa a enproceso
                    .and()

                  .withExternal().source(OrderStates.TERMINADA).target(OrderStates.NUEVAPROVI).event(OrderEvents.INICIA_PROCESO);
        }
//
//        public void configure(StateMachineTransitionConfigurer< OrderStates2, OrderEvents> transitions) throws Exception {
//            /** Se deben poner todos los caminos posibles o el estado quedaria estancado
//             *            */
////            String test="secretaria";
//            transitions
//                    .withExternal()
//                    .source(OrderStates2.inicio).target(OrderStates2.proceso).event(OrderEvents.INICIA_PROCESO)
//                  .and()
//                    .withExternal().source(OrderStates2.proceso).target(OrderStates2.elevado).event(OrderEvents.ELEVAR)
//
//                    ;
//
//
//
//        }
////



        @Bean
        public Guard<OrderStates, OrderEvents> Nguard(boolean b) {
            return new Guard<OrderStates, OrderEvents>() {

                @Override
                public boolean evaluate(StateContext<OrderStates, OrderEvents> context) {


                    return nombre.equals(apellido); //condicionar que el contexto sea el igual
                }
            };
        }
        /**
             *  configuracion del orden de los estados
             *  */
        @Override
        public void configure(StateMachineStateConfigurer<OrderStates, OrderEvents> states  ) throws Exception {
          states

                  .withStates()

//                  .initial(OrderStates.CREADA)
//                  .state(OrderStates.EN_PROCESO)
//                  .state(OrderStates.ELEVADA)
//                  .end(OrderStates.TERMINADA)
                  .initial(OrderStates.CREADA)
                  .stateEntry(OrderStates.CREADA,stateContext ->  {


                      log.info( " LO QUE PUEDE VER EL USUARIO EN CREADA");

                  })
                  .state(OrderStates.TERMINADA)
                  .stateEntry(OrderStates.TERMINADA,stateContext ->  {


                      log.info("LO QUE PUEDE VER EL USUARIO EN TERMINADA");

                  })
                 .and()
                  .withStates()

                  .state(OrderStates.NUEVAPROVI)

                  .and()
                  .withStates()
                  .state(OrderStates.NºREFERENCIA)
                  .stateEntry(OrderStates.NºREFERENCIA,stateContext ->  {


                      log.info("LO QUE PUEDE VER EL USUARIO EN NºReferencia");

                  })
                  .end(OrderStates.VALIDACION);



//                  .and()
//                  .withStates()
//                  .parent(OrderStates.E1)
//                    .initial(OrderStates.E2)
//                  .state(OrderStates.E2);

        }

         @Override
         public void configure (StateMachineConfigurationConfigurer<OrderStates , OrderEvents> config) throws Exception{

            StateMachineListenerAdapter<OrderStates , OrderEvents> adapter = new StateMachineListenerAdapter<OrderStates , OrderEvents>(){
            @Override
                        public void stateChanged(State <OrderStates, OrderEvents> from, State<OrderStates, OrderEvents> to) {

                           //log.info(String.format("stateChanged(from: %s, to: %s)",from+"",to+""));
                           //super.stateChanged(from,to);
                        }
                    };
                        config.withConfiguration()
                                .autoStartup(false)
                                .listener(adapter);

                    }

        }
	


